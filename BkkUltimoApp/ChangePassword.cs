﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BkkUltimoApp
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            String usernamebox = textBox1.Text;
            String oldpasswordbox = textBox2.Text;
            String newpasswordbox = textBox3.Text;
            String confirmnewpass = textBox4.Text;

            if (usernamebox == "" || oldpasswordbox == "" || newpasswordbox == "" || confirmnewpass == "")
            {
                MessageBox.Show("All fields are required. please fill them.");
            }

            else if (newpasswordbox != confirmnewpass)
            {
                MessageBox.Show("the new password you enter do not match please retry", "password mismatch", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {

                SQLiteDatabase db = new SQLiteDatabase();
                Dictionary<String, String> data = new Dictionary<String, String>();

                data.Add("username", usernamebox);
                data.Add("password", newpasswordbox);
                String cond = "username = '" + usernamebox + "' and password = '" + oldpasswordbox + "'";

                try
                {

                    if (db.Update("usertable", data, cond))
                    {
                        MessageBox.Show("Your password has been successfully changed.");
                    }
                    else
                    {
                        MessageBox.Show("Your password was not changed. \n check the username and password entered");
                    }

                }

                catch (Exception crap)
                {

                    MessageBox.Show(crap.Message);

                }
                finally
                {
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                
                }
            }
        }
    }
}
