﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace BkkUltimoApp
{
    public partial class NewCustomer : Form
    {
        bool flag = false;
        public NewCustomer()
        {
            InitializeComponent();
        }

        private void NewCustomer_Load(object sender, EventArgs e)
        {
            loaddesigner();
        }

        private void loaddesigner()
        {
            try
            {

                SQLiteDatabase db = new SQLiteDatabase();

                DataTable designer1;

                String query = "select fname from designertable";


                designer1 = db.GetDataTable(query);

                foreach (DataRow r in designer1.Rows)
                {
                    designer.Items.Add(r["fname"].ToString());
                }

                

            }

            catch (Exception fail)
            {

                String error = "The following error has occurred:\n\n";

                error += fail.Message.ToString() + "\n\n";

                MessageBox.Show(error);

                this.Close();

            }
        
        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            fname.Text = "";
            lname.Text = "";
            phone.Text = "";
            address.Text = "";
            clothcolor.Text = "";
            clothtype.Text = "";
            style.Text = "";
            duration.Text = "";
            durationlevel.Text = "";
            amount.Text = "";
            designer.Text = "";
            bd.Text = "";
            hl.Text = "";
            tl.Text = "";
            nk.Text = "";
            ag.Text = "";
            lt.Text = "";
            cap.Text = "";
           

        }

        private int getnextcusid()
        {
            int rcid = 0;
            try
            {

                SQLiteDatabase db = new SQLiteDatabase();
                DataTable cusidtab;
                String query = "select max(cusid) id from customerInfo";
                cusidtab = db.GetDataTable(query);
                String cid= "";
                foreach (DataRow r in cusidtab.Rows)
                {
                    cid = r["id"].ToString();
                }
                rcid = Int32.Parse(cid);
                rcid++;
            }

            catch (Exception fail)
            {

                String error = "The following error has occurred:\n\n";

                error += fail.Message.ToString() + "\n\n";

                MessageBox.Show(error);

                this.Close();

            }

            return rcid;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (fname.Text == "" || lname.Text == "" || clothcolor.Text == "" || clothtype.Text == "" || style.Text == "" || duration.Text == "0" || durationlevel.Text == "" || amount.Text == "" || designer.Text == "" || bd.Text == "" || hl.Text == "" || tl.Text == "" || nk.Text == "" || ag.Text == "" || lt.Text == "" || cap.Text == "")
            {
                MessageBox.Show("All fields are required. fill them");
            }
            else
            {
                SQLiteDatabase db = new SQLiteDatabase();

                Dictionary<String, String> data = new Dictionary<String, String>();

                data.Add("fname", fname.Text);
                data.Add("lname", lname.Text);
                data.Add("phone", phone.Text);
                data.Add("address", address.Text);
                data.Add("color", clothcolor.Text);
                data.Add("clothtype", clothtype.Text);
                data.Add("duration", duration.Text + durationlevel.Text);
                data.Add("designer", designer.Text);
                data.Add("amount", amount.Text);
                data.Add("bd", bd.Text);
                data.Add("hl", hl.Text);
                data.Add("tl", tl.Text);
                data.Add("nk", nk.Text);
                data.Add("ag", ag.Text);
                data.Add("lt", lt.Text);
                data.Add("cap", cap.Text);
                data.Add("cusid", getnextcusid().ToString());
                data.Add("style", style.Text);

                try
                {

                    if (db.Insert("customerInfo", data))
                    {
                        MessageBox.Show("Customer information successfully saved.");
                        flag = true;
                    }

                }

                catch (Exception crap)
                {

                    MessageBox.Show(crap.Message);
                    flag = false;

                }
               
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if(fname.Text == "" || lname.Text == "" || clothcolor.Text == "" || clothtype.Text == "" || style.Text == "" || duration.Text == "0" || durationlevel.Text == "" || amount.Text == "" || designer.Text == "" || bd.Text == "" || hl.Text == "" || tl.Text == "" || nk.Text == "" || ag.Text == "" || lt.Text == "" || cap.Text == "")
            {
                MessageBox.Show("Please Enter details before you print receipt");
            }
            else if (!flag)
            {
                MessageBox.Show("Please Save information before printing");
            }
            else
            {
                printPreviewDialog1.ShowDialog();
            }
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            // Create image.
            Image newImage = Image.FromFile("com logo.jpg");
            Image yourImage = resizeImage(newImage, new Size(150, 150));
            // Create point for upper-left corner of image.
            PointF ulCorner = new PointF(650.0F, 50.0F);

           


            String hed = "         BKK ULTIMO\n";
            hed += "CUSTOMER RECEIPT";
            String msg = "\n\n";
           msg += "Customer Name:\t " + fname.Text + " " + lname.Text;
           msg += "\nCloth Type:\t " + clothtype.Text;
           msg += "\nStyle:\t \t " + style.Text;
           msg += "\nDesigner Name:\t " + designer.Text;
           msg += "\nDuration:\t " + duration.Text + durationlevel.Text;
           msg += "\nAmount Charged: =N=" + amount.Text;

            Graphics g = e.Graphics;
            String message = System.Environment.UserName;
            Font messageFont = new Font("Arial",
                     24, System.Drawing.GraphicsUnit.Point);
            Font messageFont1 = new Font("Arial",
                     34, System.Drawing.GraphicsUnit.Point);
            g.DrawImage(yourImage, ulCorner);
            g.DrawString(hed, messageFont1, Brushes.Black, 150, 100);
            g.DrawString(msg, messageFont, Brushes.Black, 150, 200);
            g.DrawString("________________ \n FOR BKK ULTIMO", messageFont, Brushes.Black, 530, 530);
           

        }

        private void printPreviewDialog1_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

     public static Image resizeImage(Image imgToResize, Size size)
    {
       return (Image)(new Bitmap(imgToResize, size));
    }

    
    }
}
