﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BkkUltimoApp
{
    public partial class AllCustomers : Form
    {
        public AllCustomers()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void AllCustomers_Load(object sender, EventArgs e)
        {
            try
            {

                SQLiteDatabase db = new SQLiteDatabase();

                DataTable designer;

                String query = "select distinct fname First_Name,lname Last_Name,phone Phone from customerInfo";


                designer = db.GetDataTable(query);

                // The results can be directly applied to a DataGridView control

                dataGridView1.DataSource = designer;



            }

            catch (Exception fail)
            {

                String error = "The following error has occurred:\n\n";

                error += fail.Message.ToString() + "\n\n";

                MessageBox.Show(error);

                this.Close();

            }

        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            // Create image.
            Image newImage = Image.FromFile("com logo.jpg");
            Image yourImage = resizeImage(newImage, new Size(150, 150));
            // Create point for upper-left corner of image.
            PointF ulCorner = new PointF(650.0F, 50.0F);




            String hed = "    BKK ULTIMO\n";
            hed += "CUSTOMER LIST";
            String msg = "\n\n";
            String th = "FIRST_NAME \t LAST_NAME \t PHONE";
            String bodyf = "\n";
            String bodyl = "\n";
            String bodyp = "\n";
            try
            {

                SQLiteDatabase db = new SQLiteDatabase();

                DataTable designer;

                String query = "select distinct fname,lname,phone from customerInfo";
              

                designer = db.GetDataTable(query);

                // The results can be directly applied to a DataGridView control

             //   dataGridView1.DataSource = designer;

              

                foreach (DataRow r in designer.Rows)
                {

                    bodyf += r["fname"].ToString() + "\n";
                    bodyl += r["lname"].ToString() + "\n";
                    bodyp +=r["phone"].ToString() + "\n";
                }

            }

            catch (Exception fail)
            {

                String error = "The following error has occurred:\n\n";

                error += fail.Message.ToString() + "\n\n";

                MessageBox.Show(error);

                this.Close();

            }







            Graphics g = e.Graphics;
            String message = System.Environment.UserName;
            Font messageFont = new Font("Arial",
                     24, System.Drawing.GraphicsUnit.Point);
            Font messageFont1 = new Font("Arial",
                     34, System.Drawing.GraphicsUnit.Point);
            
            g.DrawImage(yourImage, ulCorner);
            g.DrawString(hed, messageFont1, Brushes.Black, 150, 100);
            g.DrawString(th, messageFont, Brushes.Black, 70, 290);
            g.DrawString(bodyf, messageFont, Brushes.Black, 70, 310);
            g.DrawString(bodyl, messageFont, Brushes.Black,340, 310);
            g.DrawString(bodyp, messageFont, Brushes.Black, 610, 310);

           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.ShowDialog();
        }

        public static Image resizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }
    }
}
