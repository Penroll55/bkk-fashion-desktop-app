﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BkkUltimoApp
{
    public partial class DeleteUser : Form
    {
        public DeleteUser()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("All Fields are required!!!");
            }
            else
            {

                if (MessageBox.Show("Are You Sure ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        String usernamebox = textBox1.Text;
                        String passwordbox = textBox2.Text;
                        SQLiteDatabase db = new SQLiteDatabase();

                        String condi = "username = '" + usernamebox + "' and password = '" + passwordbox + "'";

                        if (db.Delete("usertable", condi))
                        {
                            //sucess
                            MessageBox.Show("Software User Information deleted.");
                        }
                        else
                        {
                            // error msg
                            MessageBox.Show("Software User Information could not be deleted. \n No such user.");
                        }
                    }
                    catch (Exception crap)
                    {
                        MessageBox.Show(crap.Message);
                    }
                    finally
                    {
                        textBox1.Text = "";
                        textBox2.Text = "";
                    }

                }
            }
        }
    }
}
