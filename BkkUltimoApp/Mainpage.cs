﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BkkUltimoApp
{
    public partial class Mainpage : Form
    {
        public Mainpage()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = DateTime.Now.ToLongTimeString();
        }

        private void logOutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Login lg = new Login();
            lg.Show();
            this.Close();
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (MessageBox.Show("Are You Sure ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.MdiChildren.Count()>=1)
            {
               
                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            
            AddDesigner addsgn = new AddDesigner();
            addsgn.MdiParent = this;
            addsgn.StartPosition = FormStartPosition.CenterScreen;
            addsgn.Show();
            
        }

      

        private void addNewDesignerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            AddDesigner addsgn = new AddDesigner();
            addsgn.MdiParent = this;
            addsgn.StartPosition = FormStartPosition.CenterScreen;
            addsgn.Show();
            
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            AddUser adduser = new AddUser();
            adduser.MdiParent = this;
            adduser.StartPosition = FormStartPosition.CenterScreen;
            adduser.Show();
            
        }

        private void addUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            AddUser adduser = new AddUser();
            adduser.MdiParent = this;
            adduser.StartPosition = FormStartPosition.CenterScreen;
            adduser.Show();
            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            ChangePassword cp = new ChangePassword();
            cp.MdiParent = this;
            cp.StartPosition = FormStartPosition.CenterScreen;
            cp.Show();
            
        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            ChangePassword cp = new ChangePassword();
            cp.MdiParent = this;
            cp.StartPosition = FormStartPosition.CenterScreen;
            cp.Show();
        }

        private void deleteUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            DeleteUser deluser = new DeleteUser();
            deluser.MdiParent = this;
            deluser.StartPosition = FormStartPosition.CenterScreen;
            deluser.Show();
        }

        private void viewAllDesignersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            AllDesigner alldesn = new AllDesigner();
            alldesn.MdiParent = this;
            alldesn.StartPosition = FormStartPosition.CenterScreen;
            alldesn.Show();
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            AllDesigner alldesn = new AllDesigner();
            alldesn.MdiParent = this;
            alldesn.StartPosition = FormStartPosition.CenterScreen;
            alldesn.Show();
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            AllDesigner alldesn = new AllDesigner();
            alldesn.MdiParent = this;
            alldesn.StartPosition = FormStartPosition.CenterScreen;
            alldesn.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {
           
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            NewCustomer newc = new NewCustomer();
            newc.MdiParent = this;
            newc.StartPosition = FormStartPosition.CenterScreen;
            newc.Show();
        }

        private void commodityReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
             if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            NewCustomer newc = new NewCustomer();
            newc.MdiParent = this;
            newc.StartPosition = FormStartPosition.CenterScreen;
            newc.Show();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            OldCustomer oldc = new OldCustomer();
            oldc.MdiParent = this;
            oldc.StartPosition = FormStartPosition.CenterScreen;
            oldc.Show();
        }

        private void feeReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            OldCustomer oldc = new OldCustomer();
            oldc.MdiParent = this;
            oldc.StartPosition = FormStartPosition.CenterScreen;
            oldc.Show();
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            AllCustomers alcus = new AllCustomers();
            alcus.MdiParent = this;
            alcus.StartPosition = FormStartPosition.CenterScreen;
            alcus.Show();

        }

        private void viewAllStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }
            AllCustomers alcus = new AllCustomers();
            alcus.MdiParent = this;
            alcus.StartPosition = FormStartPosition.CenterScreen;
            alcus.Show();
        }

        private void manageDesignerRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Count() >= 1)
            {

                Mainpage.ActiveForm.ActiveMdiChild.Close();
            }

            DesignerRecord dr = new DesignerRecord();
            dr.MdiParent = this;
            dr.StartPosition = FormStartPosition.CenterScreen;
            dr.Show();
        }
    }
}
