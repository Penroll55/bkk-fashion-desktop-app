﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BkkUltimoApp
{
    public partial class AddDesigner : Form
    {
        public AddDesigner()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            fname.Text = "";
            lname.Text = "";
            phone.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
             if (fname.Text == "" || lname.Text == "" || phone.Text == "")
            {
                MessageBox.Show("All fields are required. please fill them.");
            }
            else
            {
                SQLiteDatabase db = new SQLiteDatabase();

                Dictionary<String, String> data = new Dictionary<String, String>();

                data.Add("fname", fname.Text);

                data.Add("lname", lname.Text);

                data.Add("phone", phone.Text);
              try
                {

                    if (db.Insert("designertable", data))
                    {
                        MessageBox.Show("Designer information added successfully.");
                    }

                }

                catch (Exception crap)
                {

                    MessageBox.Show(crap.Message);

                }
                finally
                {
                    fname.Text = "";
                    lname.Text = "";
                    phone.Text = "";
                }
            }
            
      
        }
    }
}
