﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BkkUltimoApp
{
    public partial class DesignerRecord : Form
    {
        public DesignerRecord()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            fname.Text = "";
            lname.Text = "";
            phone.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (fname.Text == "" || lname.Text == "" || phone.Text == "" || textBox1.Text == "")
            {
                MessageBox.Show("All fields are required. fill them");
            }
            else
            {
                SQLiteDatabase db = new SQLiteDatabase();

                Dictionary<String, String> data = new Dictionary<String, String>();

                data.Add("fname", fname.Text);
                data.Add("lname", lname.Text);
                data.Add("phone", phone.Text);

                String whercond = "id=" + textBox1.Text;

                try
                {

                    if (db.Update("designertable", data, whercond))
                    {
                        MessageBox.Show("Designer information updated successfully.");
                    }
                    else
                    {
                        MessageBox.Show("No Record updated. check the details supplied!");
                    }

                }

                catch (Exception crap)
                {

                    MessageBox.Show(crap.Message);

                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
           

            if (textBox1.Text=="")
            {
                MessageBox.Show("Please Enter Designer ID to search.");
            }
            else
            {
                String qry = "select fname,lname,phone from designertable";
                qry += " where id = '" + textBox1.Text + "' limit 1";
                try
                {

                    SQLiteDatabase db = new SQLiteDatabase();

                    DataTable designer;

                    designer = db.GetDataTable(qry);

                    // The results can be directly applied to a DataGridView control
                    if (designer.Rows.Count == 1)
                    {
                        foreach (DataRow r in designer.Rows)
                        {
                            fname.Text = r["fname"].ToString();
                            lname.Text = r["lname"].ToString();
                            phone.Text = r["phone"].ToString();
                          
                        }
                    }
                    else
                    {
                        MessageBox.Show("Designer Information not found!!! \n check the ID that was entered. ");
                    }



                }

                catch (Exception fail)
                {

                    String error = "The following error has occurred:\n\n";

                    error += fail.Message.ToString() + "\n\n";

                    MessageBox.Show(error);


                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Please Enter the ID");
            }
            else
            {

                if (MessageBox.Show("Are You Sure ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        String id = textBox1.Text;

                        SQLiteDatabase db = new SQLiteDatabase();

                        String condi = "id = '" + id + "'";

                        if (db.Delete("designertable", condi))
                        {
                            //sucess
                            MessageBox.Show("Designer Information deleted.");
                        }
                        else
                        {
                            // error msg
                            MessageBox.Show("Designer Information could not be deleted. \n No such user.");
                        }
                    }
                    catch (Exception crap)
                    {
                        MessageBox.Show(crap.Message);
                    }
                    finally
                    {
                        textBox1.Text = "";
                        fname.Text = "";
                        lname.Text = "";
                        phone.Text = "";

                    }

                }

            }
        }
    }
}
