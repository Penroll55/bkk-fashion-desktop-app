﻿namespace BkkUltimoApp
{
    partial class OldCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OldCustomer));
            this.label12 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.hlabel = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.amount = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.clothcolor = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.designer = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.durationlevel = new System.Windows.Forms.ComboBox();
            this.duration = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.style = new System.Windows.Forms.ComboBox();
            this.clothtype = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cap = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ag = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.hl = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tl = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.nk = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bd = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.address = new System.Windows.Forms.RichTextBox();
            this.phone = new System.Windows.Forms.TextBox();
            this.lname = new System.Windows.Forms.TextBox();
            this.fname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(256, 15);
            this.label12.TabIndex = 14;
            this.label12.Text = "Enter Customer First and Last name to search";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Black;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Firebrick;
            this.button3.Location = new System.Drawing.Point(271, 75);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(65, 40);
            this.button3.TabIndex = 13;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Firebrick;
            this.button2.Location = new System.Drawing.Point(313, 444);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(156, 40);
            this.button2.TabIndex = 12;
            this.button2.Text = "New Design request";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Firebrick;
            this.button1.Location = new System.Drawing.Point(116, 444);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(193, 40);
            this.button1.TabIndex = 11;
            this.button1.Text = "view previous transactions";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Black;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Firebrick;
            this.button4.Location = new System.Drawing.Point(10, 444);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(104, 40);
            this.button4.TabIndex = 10;
            this.button4.Text = "Update Info";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.EnabledChanged += new System.EventHandler(this.button4_EnabledChanged);
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.Blue;
            this.textBox1.Location = new System.Drawing.Point(200, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(367, 38);
            this.textBox1.TabIndex = 5;
            this.textBox1.Text = "Old Customer Information";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.hlabel);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.address);
            this.groupBox1.Controls.Add(this.phone);
            this.groupBox1.Controls.Add(this.lname);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.fname);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox1.Location = new System.Drawing.Point(21, 76);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(757, 351);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "....::Customer Info";
            // 
            // hlabel
            // 
            this.hlabel.AutoSize = true;
            this.hlabel.Location = new System.Drawing.Point(349, 19);
            this.hlabel.Name = "hlabel";
            this.hlabel.Size = new System.Drawing.Size(0, 18);
            this.hlabel.TabIndex = 15;
            this.hlabel.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.amount);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.clothcolor);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.designer);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.durationlevel);
            this.groupBox3.Controls.Add(this.duration);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.style);
            this.groupBox3.Controls.Add(this.clothtype);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox3.Location = new System.Drawing.Point(23, 181);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(502, 165);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "...:::Sowing Details:";
            this.groupBox3.Visible = false;
            // 
            // amount
            // 
            this.amount.Location = new System.Drawing.Point(351, 107);
            this.amount.Name = "amount";
            this.amount.Size = new System.Drawing.Size(121, 24);
            this.amount.TabIndex = 21;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(260, 112);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 18);
            this.label17.TabIndex = 20;
            this.label17.Text = "Amount:";
            // 
            // clothcolor
            // 
            this.clothcolor.Location = new System.Drawing.Point(351, 33);
            this.clothcolor.Name = "clothcolor";
            this.clothcolor.Size = new System.Drawing.Size(121, 24);
            this.clothcolor.TabIndex = 19;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(257, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 18);
            this.label16.TabIndex = 18;
            this.label16.Text = "Cloth Color:";
            // 
            // designer
            // 
            this.designer.FormattingEnabled = true;
            this.designer.Location = new System.Drawing.Point(351, 70);
            this.designer.Name = "designer";
            this.designer.Size = new System.Drawing.Size(121, 26);
            this.designer.TabIndex = 17;
            this.designer.Text = "--Select Designer--";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(257, 73);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 18);
            this.label15.TabIndex = 16;
            this.label15.Text = "Designer:";
            // 
            // durationlevel
            // 
            this.durationlevel.FormattingEnabled = true;
            this.durationlevel.Items.AddRange(new object[] {
            "Days",
            "Weeks"});
            this.durationlevel.Location = new System.Drawing.Point(159, 105);
            this.durationlevel.Name = "durationlevel";
            this.durationlevel.Size = new System.Drawing.Size(68, 26);
            this.durationlevel.TabIndex = 15;
            // 
            // duration
            // 
            this.duration.Location = new System.Drawing.Point(105, 105);
            this.duration.Name = "duration";
            this.duration.Size = new System.Drawing.Size(48, 24);
            this.duration.TabIndex = 14;
            this.duration.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 108);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 18);
            this.label14.TabIndex = 6;
            this.label14.Text = "Duration:";
            // 
            // style
            // 
            this.style.FormattingEnabled = true;
            this.style.Items.AddRange(new object[] {
            "Agbada",
            "Dongari",
            "Buba and Sokoto",
            "Others"});
            this.style.Location = new System.Drawing.Point(106, 65);
            this.style.Name = "style";
            this.style.Size = new System.Drawing.Size(121, 26);
            this.style.TabIndex = 5;
            this.style.Text = "--Select Style--";
            // 
            // clothtype
            // 
            this.clothtype.FormattingEnabled = true;
            this.clothtype.Items.AddRange(new object[] {
            "Ankara",
            "Lace",
            "Guinea",
            "Materials ",
            "Others"});
            this.clothtype.Location = new System.Drawing.Point(106, 32);
            this.clothtype.Name = "clothtype";
            this.clothtype.Size = new System.Drawing.Size(121, 26);
            this.clothtype.TabIndex = 4;
            this.clothtype.Text = "--Select Type--";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 68);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 18);
            this.label13.TabIndex = 2;
            this.label13.Text = "Style:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cloth Type:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cap);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.ag);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.hl);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lt);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.tl);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.nk);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.bd);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox2.Location = new System.Drawing.Point(538, 23);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(207, 323);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "....::Measurement Details";
            // 
            // cap
            // 
            this.cap.Location = new System.Drawing.Point(52, 246);
            this.cap.Name = "cap";
            this.cap.Size = new System.Drawing.Size(48, 24);
            this.cap.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 249);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 18);
            this.label11.TabIndex = 12;
            this.label11.Text = "CAP:";
            // 
            // ag
            // 
            this.ag.Location = new System.Drawing.Point(52, 171);
            this.ag.Name = "ag";
            this.ag.Size = new System.Drawing.Size(48, 24);
            this.ag.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 177);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 18);
            this.label10.TabIndex = 10;
            this.label10.Text = "AG:";
            // 
            // hl
            // 
            this.hl.Location = new System.Drawing.Point(52, 63);
            this.hl.Name = "hl";
            this.hl.Size = new System.Drawing.Size(48, 24);
            this.hl.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 18);
            this.label9.TabIndex = 8;
            this.label9.Text = "HL:";
            // 
            // lt
            // 
            this.lt.Location = new System.Drawing.Point(52, 210);
            this.lt.Name = "lt";
            this.lt.Size = new System.Drawing.Size(48, 24);
            this.lt.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 18);
            this.label8.TabIndex = 6;
            this.label8.Text = "LT:";
            // 
            // tl
            // 
            this.tl.Location = new System.Drawing.Point(52, 96);
            this.tl.Name = "tl";
            this.tl.Size = new System.Drawing.Size(48, 24);
            this.tl.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 18);
            this.label7.TabIndex = 4;
            this.label7.Text = "TL:";
            // 
            // nk
            // 
            this.nk.Location = new System.Drawing.Point(52, 137);
            this.nk.Name = "nk";
            this.nk.Size = new System.Drawing.Size(48, 24);
            this.nk.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 18);
            this.label6.TabIndex = 2;
            this.label6.Text = "NK:";
            // 
            // bd
            // 
            this.bd.Location = new System.Drawing.Point(52, 26);
            this.bd.Name = "bd";
            this.bd.Size = new System.Drawing.Size(48, 24);
            this.bd.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 18);
            this.label5.TabIndex = 0;
            this.label5.Text = "BD:";
            // 
            // address
            // 
            this.address.Location = new System.Drawing.Point(352, 51);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(157, 96);
            this.address.TabIndex = 7;
            this.address.Text = "";
            // 
            // phone
            // 
            this.phone.Location = new System.Drawing.Point(128, 123);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(136, 24);
            this.phone.TabIndex = 6;
            // 
            // lname
            // 
            this.lname.Location = new System.Drawing.Point(128, 80);
            this.lname.Name = "lname";
            this.lname.Size = new System.Drawing.Size(136, 24);
            this.lname.TabIndex = 5;
            // 
            // fname
            // 
            this.fname.Location = new System.Drawing.Point(128, 45);
            this.fname.Name = "fname";
            this.fname.Size = new System.Drawing.Size(136, 24);
            this.fname.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(280, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Address:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Phone:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Last Name:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 48);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 18);
            this.label18.TabIndex = 0;
            this.label18.Text = "First Name:";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Black;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Firebrick;
            this.button5.Location = new System.Drawing.Point(471, 445);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(170, 40);
            this.button5.TabIndex = 16;
            this.button5.Text = "Save and Print Receipt";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Black;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.Firebrick;
            this.button6.Location = new System.Drawing.Point(713, 445);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(70, 40);
            this.button6.TabIndex = 17;
            this.button6.Text = "Clear All";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            this.printPreviewDialog1.Load += new System.EventHandler(this.printPreviewDialog1_Load);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Black;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.Firebrick;
            this.button7.Location = new System.Drawing.Point(643, 445);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(67, 40);
            this.button7.TabIndex = 18;
            this.button7.Text = "Delete";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // OldCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(798, 495);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OldCustomer";
            this.Text = "OldCustomer";
            this.Load += new System.EventHandler(this.OldCustomer_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox amount;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox clothcolor;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox designer;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox durationlevel;
        private System.Windows.Forms.TextBox duration;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox style;
        private System.Windows.Forms.ComboBox clothtype;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox cap;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox ag;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox hl;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox lt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox nk;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox bd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox address;
        private System.Windows.Forms.TextBox phone;
        private System.Windows.Forms.TextBox lname;
        private System.Windows.Forms.TextBox fname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label hlabel;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Button button7;
    }
}