﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BkkUltimoApp
{
    public partial class AllDesigner : Form
    {
        public AllDesigner()
        {
            InitializeComponent();
        }

        private void AllDesigner_Load(object sender, EventArgs e)
        {
            try
            {

                SQLiteDatabase db = new SQLiteDatabase();

                DataTable designer;

                String query = "select id ID, fname FIRSTNAME, lname LASTNAME, phone PHONE from designertable";


                designer = db.GetDataTable(query);

                // The results can be directly applied to a DataGridView control

                dataGridView1.DataSource = designer;
               
               

            }

            catch (Exception fail)
            {

                String error = "The following error has occurred:\n\n";

                error += fail.Message.ToString() + "\n\n";

                MessageBox.Show(error);

                this.Close();

            }

        }
    }
}
