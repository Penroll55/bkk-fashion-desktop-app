﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace BkkUltimoApp
{
    public partial class AddUser : Form
    {
        public AddUser()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            fname.Text = "";
            lname.Text = "";
            phone.Text = "";
            username.Text = "";
            password.Text = "";
            retypepassword.Text = "";
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (fname.Text == "" || lname.Text == "" || phone.Text == "" || username.Text == "" || password.Text == "" || retypepassword.Text=="")
            {
                MessageBox.Show("All fields are required. please fill them.");
            }
            else if (password.Text != retypepassword.Text)
            {
                MessageBox.Show("Password did not match. please enter again");
            }
            else
            {
                SQLiteDatabase db = new SQLiteDatabase();

                Dictionary<String, String> data = new Dictionary<String, String>();

                data.Add("fname", fname.Text);

                data.Add("lname", lname.Text);

                data.Add("phone", phone.Text);

                data.Add("username", username.Text);

                data.Add("password", password.Text);

                try
                {

                    if (db.Insert("usertable", data))
                    {
                        MessageBox.Show("Software User information submited successfully.");
                    }

                }

                catch (Exception crap)
                {

                    MessageBox.Show(crap.Message);

                }
                finally
                {
                    fname.Text = "";
                    lname.Text = "";
                    phone.Text = "";
                    username.Text = "";
                    password.Text = "";
                    retypepassword.Text = "";
                }
            }
            
        }
    }
}
