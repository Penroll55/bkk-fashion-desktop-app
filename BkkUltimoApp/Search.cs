﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BkkUltimoApp
{
    public partial class Search : Form
    {
        String id, cusname;
        public Search()
        {
            InitializeComponent();
            
        }
        public Search(String myid, String cname)
        {
            id = myid;
            cusname = cname;
            InitializeComponent();
        }

        private void Search_Load(object sender, EventArgs e)
        {
            label2.Text += " "+cusname;
            try
            {

                SQLiteDatabase db = new SQLiteDatabase();

                DataTable designer;

                String query = "select style,designer,clothtype,duration,date,amount,color from customerInfo where cusid = "+id;


                designer = db.GetDataTable(query);

                // The results can be directly applied to a DataGridView control

                dataGridView1.DataSource = designer;



            }

            catch (Exception fail)
            {

                String error = "The following error has occurred:\n\n";

                error += fail.Message.ToString() + "\n\n";

                MessageBox.Show(error);

                this.Close();

            }

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Search_Leave(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
