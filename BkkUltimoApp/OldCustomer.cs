﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BkkUltimoApp
{
    public partial class OldCustomer : Form
    {
        public OldCustomer()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String fnam = fname.Text;
            String lnam = lname.Text;
           
            if (fnam == "" || lnam == "")
            {
                MessageBox.Show("Please Enter First name and last name of cutomer to search.");
            }
            else
            {
                String qry = "select fname,lname,phone,address,bd,hl,tl,nk,ag,lt,cap,cusid from customerInfo";
                qry += " where fname like '" + fnam + "%' and lname like '" + lnam + "%' limit 1";
                try
                {

                    SQLiteDatabase db = new SQLiteDatabase();

                    DataTable designer;

                    designer = db.GetDataTable(qry);

                    // The results can be directly applied to a DataGridView control
                    if (designer.Rows.Count == 1)
                    {
                        foreach (DataRow r in designer.Rows)
                        {
                            fname.Text = r["fname"].ToString();
                            lname.Text = r["lname"].ToString();
                            phone.Text = r["phone"].ToString();
                            address.Text = r["address"].ToString();
                            bd.Text = r["bd"].ToString();
                            hl.Text = r["hl"].ToString();
                            tl.Text = r["tl"].ToString();
                            nk.Text = r["nk"].ToString();
                            ag.Text = r["ag"].ToString();
                            lt.Text = r["lt"].ToString();
                            cap.Text = r["cap"].ToString();
                            hlabel.Text = r["cusid"].ToString();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Customer Information not found!!! \n check the first name and last name entered. ");
                    }



                }

                catch (Exception fail)
                {

                    String error = "The following error has occurred:\n\n";

                    error += fail.Message.ToString() + "\n\n";

                    MessageBox.Show(error);


                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            groupBox3.Visible = true;
            button4.Enabled = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            fname.Text = "";
            lname.Text = "";
            phone.Text = "";
            address.Text = "";
            clothcolor.Text = "";
            clothtype.Text = "";
            style.Text = "";
            duration.Text = "";
            durationlevel.Text = "";
            amount.Text = "";
            designer.Text = "";
            bd.Text = "";
            hl.Text = "";
            tl.Text = "";
            nk.Text = "";
            ag.Text = "";
            lt.Text = "";
            cap.Text = "";
        }

        private void OldCustomer_Load(object sender, EventArgs e)
        {
            try
            {

                SQLiteDatabase db = new SQLiteDatabase();

                DataTable designer1;

                String query = "select fname from designertable";


                designer1 = db.GetDataTable(query);

                foreach (DataRow r in designer1.Rows)
                {
                    designer.Items.Add(r["fname"].ToString());
                }



            }

            catch (Exception fail)
            {

                String error = "The following error has occurred:\n\n";

                error += fail.Message.ToString() + "\n\n";

                MessageBox.Show(error);

            

            }
        
        }

        private void button4_Click(object sender, EventArgs e)
        {
           
            if (fname.Text == "" || lname.Text == "" || bd.Text == "" || hl.Text == "" || tl.Text == "" || nk.Text == "" || ag.Text == "" || lt.Text == "" || cap.Text == "")
            {
                MessageBox.Show("All fields are required. fill them");
            }
            else
            {
                SQLiteDatabase db = new SQLiteDatabase();

                Dictionary<String, String> data = new Dictionary<String, String>();

                data.Add("fname", fname.Text);
                data.Add("lname", lname.Text);
                data.Add("phone", phone.Text);
                data.Add("address", address.Text);
                data.Add("bd", bd.Text);
                data.Add("hl", hl.Text);
                data.Add("tl", tl.Text);
                data.Add("nk", nk.Text);
                data.Add("ag", ag.Text);
                data.Add("lt", lt.Text);
                data.Add("cap", cap.Text);
                String whercond = "cusid="+hlabel.Text;
                          
             try
                {

                    if (db.Update("customerInfo", data, whercond))
                    {
                        MessageBox.Show("Customer information updated successfully.");
                    }
                    else
                    {
                        MessageBox.Show("No Record updated. check the details supplied!");
                    }

                }

                catch (Exception crap)
                {

                    MessageBox.Show(crap.Message);

                }

            }
        }

        private void button4_EnabledChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Please use Save and Print Receipt Button to Enter new Design information into the system.");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(hlabel.Text=="" ||fname.Text=="" ||lname.Text=="")
            {
                MessageBox.Show("Please get Customer Names by Search");
            }
            else
            {
              
             Search search = new Search(hlabel.Text,fname.Text+" "+lname.Text);
             search.MdiParent = this.MdiParent;
             search.StartPosition = FormStartPosition.CenterScreen;
             search.Show();
            }
           
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if(savetransaction())
            printPreviewDialog1.ShowDialog();

            
        }
        private bool savetransaction()
        {
            if (fname.Text == "" || lname.Text == "" || clothcolor.Text == "" || clothtype.Text == "" || style.Text == "" || duration.Text == "0" || durationlevel.Text == "" || amount.Text == "" || designer.Text == "" || bd.Text == "" || hl.Text == "" || tl.Text == "" || nk.Text == "" || ag.Text == "" || lt.Text == "" || cap.Text == "")
            {
                MessageBox.Show("All fields are required. \n use search to get customer information and fill the sowing details for the new request");
                return false;
            }
            else
            {
                SQLiteDatabase db = new SQLiteDatabase();

                Dictionary<String, String> data = new Dictionary<String, String>();

                data.Add("fname", fname.Text);
                data.Add("lname", lname.Text);
                data.Add("phone", phone.Text);
                data.Add("address", address.Text);
                data.Add("color", clothcolor.Text);
                data.Add("clothtype", clothtype.Text);
                data.Add("duration", duration.Text + durationlevel.Text);
                data.Add("designer", designer.Text);
                data.Add("amount", amount.Text);
                data.Add("bd", bd.Text);
                data.Add("hl", hl.Text);
                data.Add("tl", tl.Text);
                data.Add("nk", nk.Text);
                data.Add("ag", ag.Text);
                data.Add("lt", lt.Text);
                data.Add("cap", cap.Text);
                data.Add("style", style.Text);
                data.Add("cusid", hlabel.Text);

                try
                {

                    if (db.Insert("customerInfo", data))
                    {
                        MessageBox.Show("New sowing information saved. \n please wait while receipt is been printed");
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Sowing Request could not be saved. please check to make sure this customer really exist");
                        return false;
                    }

                }

                catch (Exception crap)
                {

                    MessageBox.Show(crap.Message);
                    return false;

                }

            }
        }

        private void reset()
        {
            fname.Text = "";
            lname.Text = "";
            phone.Text = "";
            address.Text = "";
            clothcolor.Text = "";
            clothtype.Text = "";
            style.Text = "";
            duration.Text = "";
            durationlevel.Text = "";
            amount.Text = "";
            designer.Text = "";
            bd.Text = "";
            hl.Text = "";
            tl.Text = "";
            nk.Text = "";
            ag.Text = "";
            lt.Text = "";
            cap.Text = "";
            hlabel.Text = "";
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            // Create image.
            Image newImage = Image.FromFile("com logo.jpg");
            Image yourImage = resizeImage(newImage, new Size(150, 150));
            // Create point for upper-left corner of image.
            PointF ulCorner = new PointF(650.0F, 50.0F);




            String hed = "         BKK ULTIMO\n";
            hed += "CUSTOMER RECEIPT";
            String msg = "\n\n";
            msg += "Customer Name:\t " + fname.Text + " " + lname.Text;
            msg += "\nCloth Type:\t " + clothtype.Text;
            msg += "\nStyle:\t \t " + style.Text;
            msg += "\nDesigner Name:\t " + designer.Text;
            msg += "\nDuration:\t " + duration.Text + durationlevel.Text;
            msg += "\nAmount Charged: =N=" + amount.Text;

            Graphics g = e.Graphics;
            String message = System.Environment.UserName;
            Font messageFont = new Font("Arial",
                     24, System.Drawing.GraphicsUnit.Point);
            Font messageFont1 = new Font("Arial",
                     34, System.Drawing.GraphicsUnit.Point);
            g.DrawImage(yourImage, ulCorner);
            g.DrawString(hed, messageFont1, Brushes.Black, 150, 100);
            g.DrawString(msg, messageFont, Brushes.Black, 150, 200);
            g.DrawString("________________ \n FOR BKK ULTIMO", messageFont, Brushes.Black, 530, 530);
        }

        public static Image resizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (fname.Text == "" || lname.Text == "" || bd.Text == "" || hl.Text == "" || tl.Text == "" || nk.Text == "" || ag.Text == "" || lt.Text == "" || cap.Text == "")
            {
                MessageBox.Show("All fields are required. \n Use search to get the details");
            }
            else
            {

                if (MessageBox.Show("Are You Sure ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        String id = hlabel.Text;

                        SQLiteDatabase db = new SQLiteDatabase();

                        String condi = "cusid = '" + id + "'";

                        if (db.Delete("customerInfo", condi))
                        {
                            //sucess
                            MessageBox.Show("Customer Information deleted.");
                        }
                        else
                        {
                            // error msg
                            MessageBox.Show("Customer Information could not be deleted. \n No such user.");
                        }
                    }
                    catch (Exception crap)
                    {
                        MessageBox.Show(crap.Message);
                    }
                    finally
                    {
                        reset();

                    }

                }

            }
        }

        private void printPreviewDialog1_Load(object sender, EventArgs e)
        {

        }
    }
}
